return {
  filetypes = {
    "html",
    "htmldjango",
    "typescriptreact",
    "javascriptreact",
    "css",
    "sass",
    "scss",
    "less",
  }
}

