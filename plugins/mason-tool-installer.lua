return {
  ensure_installed = {
    -- LSP
    "gopls",
    "typescript-language-server",
    "pyright",
    "css-lsp",
    "ansible-language-server",
    "bash-language-server",
    "emmet-ls",
    "dockerfile-language-server",
    "html-lsp",
    "haskell-language-server",
    "vue-language-server",
    "yaml-language-server",
    "sqls",
    "taplo",
    "terraform-ls",
    "tailwindcss-language-server",
    -- Formatter
    "goimports",
    "impl",
    "prettierd",
    "blue",
    "stylua",
    "shfmt",
    "sql-formatter",
    "isort",
    -- Linter
    "djlint",
    "flake8",
    "tflint",
    "selene",
    "shellcheck",
    "hadolint",
    "sqlfluff",
    -- DAP
    "delve",
    "gotests",
  }
}
