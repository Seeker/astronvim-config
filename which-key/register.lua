-- Add bindings
return {
  -- first key is the mode, n == normal mode
  n = {
    -- second key is the prefix, <leader> prefixes
    ["<leader>"] = {
      -- which-key registration table for normal mode, leader prefix
      -- ["N"] = { "<cmd>tabnew<cr>", "New Buffer" },
      -- ["N"] = {
      --   name = "+neorg",
      --   ["S"] = { "<cmd>NeorgStart<cr>", "Start neorg" }
      -- }
      -- ["M"] = {
      --   name = "+MkdnFlow",
      --   ["M"] = { "<cmd>edit ~/wiki/index.md<cr>", "Go to notebook home" },
      -- },
      ["C"] = {
        name = "+cd",
        ["d"] = { "<cmd>cd %:p:h<cr>", "Change directory to current file" },
        ["o"] = { "<cmd>cd ~/.config/nvim/lua/user<cr>", "Change to AstroNvim options directory" },
        ["z"] = { "<cmd>ZkCd<cr>", "Change to Zettelkasten directory" },
      },
      ["n"] = {
        name = "+notes",
        ["n"] = { ":ZkNew<cr>", "New note" },
        ["f"] = { ":ZkNotes<cr>", "Open note picker" },
      },
      ["f"] = {
        ["z"] = { ":ZkNotes<cr>", "Search notes" },
      }
    },
  },
  v = {
    ["<leader>"] = {
      ["n"] = { ":'<,'>ZkNewFromTitleSelection<cr>", "New note from selection" },
      ["c"] = { ":'<,'>ZkNewFromContentSelection<cr>", "New note from selection as content"}
    },
  },
}
