<!-- vim:textwidth=80:fo+=a:
-->

# AstroNvim configuration for dummies (like me)

Hello!

If you have found this repository, you have probably just started tumbling down
the rabbit hole of Neovim customization and are a little lost trying to set up
AstroNvim to your liking.

This is going to be a fully functional and up-to-date copy of my personal
AstroNvim config with all of the comments and explanations I would've liked to
know starting out.

