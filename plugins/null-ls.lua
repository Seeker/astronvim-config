return function(config)
  local null_ls = require "null-ls"
  -- Check supported formatters and linters
  -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
  -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
  config.sources = {
    -- Set a formatter
    null_ls.builtins.formatting.rufo,
    null_ls.builtins.formatting.phpcsfixer,
    -- Set a linter
    null_ls.builtins.diagnostics.rubocop,
  }
  return config -- return final config table
end
