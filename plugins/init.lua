-- Add plugins, the packer syntax without the "use"
return {
  -- colorscheme
  ["catppuccin/nvim"] = {
    as = "catppuccin",
    config = function() require("catppuccin").setup {} end,
  },
  -- filetypes
  ["hashivim/vim-terraform"] = {}, -- Hashicorp Terraform
  ["pearofducks/ansible-vim"] = {}, -- Ansible playbooks and templates
  ["fladson/vim-kitty"] = {}, -- Kitty config files
  ["fatih/vim-go"] = {}, -- Go files
  -- note taking
  ["mickael-menu/zk-nvim"] = {
    -- module = { "zk", "zk.commands" },
    config = function()
      require("zk").setup {
        picker = "telescope",
        lsp = {
          config = {
            cmd = { "zk", "lsp" },
            name = "zk",
            on_attach = astronvim.lsp.on_attach,
            capabilities = astronvim.lsp.capabilities,
          },
          auto_attach = { enabled = true, filetypes = { "markdown" } },
        },
      }
    end,
  },
  -- other tools
  ["ellisonleao/glow.nvim"] = {}, -- Render markdown on the CLI, with pizzazz!
  ["danymat/neogen"] = { -- Generate annotations
    config = function() require("neogen").setup {} end,
    requires = "nvim-treesitter/nvim-treesitter",
  },
  ["phaazon/hop.nvim"] = { -- Gotta go fast!
    branch = "v2",
    config = function() require("hop").setup {} end,
  },
  ["lambdalisue/suda.vim"] = {}, -- tfw you forget to sudo 😩
  ["Djancyp/cheat-sheet"] = {}, -- quick cheatsheets
}
