vim.cmd "language en_US.utf8"
-- set vim options here (vim.<first_key>.<second_key> =  value)
return {
  opt = {
    relativenumber = true, -- sets vim.opt.relativenumber
    cmdheight = 1,
  },
  g = {
    mapleader = " ", -- sets vim.g.mapleader
    catppuccin_flavour = "mocha",
  },
}
